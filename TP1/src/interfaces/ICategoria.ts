export interface ICategoria 
{ //inicio de llave
    idCategoria: number;
    promocion: boolean;
    descuento: number;
    descripcion: string;
    nombreCategoria: string;
}//fin de llave