//Importar archivos o librerias
import { ICategoria } from "./interfaces/ICategoria"; //llamar interfaz main xd
import { recorrerMostrar } from "./scripts/recorrerMostrar";
//---------------------------------------------------
//---------------------------------------
//1. Definimos los datos para recorrer del arreglo
const categorias: ICategoria[] = 
[
    //coso 1
    {
      idCategoria: 1,
      promocion: false,
      descuento: 0,
      descripcion: "zapatos masculinos para adultos",
      nombreCategoria: "zapatos masculinos"
    },
];
// Llama a la función pasando el arreglo de categorías
recorrerMostrar(categorias);
