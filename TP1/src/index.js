// Importa las funciones desde buscarCategoria.js
const { buscarCategoriaPorId, mostrarCategoria } = require('./scripts/buscarCategoria');

// Ejemplo de uso de la función con un arreglo de elementos
const categoria = [
    {
        idCategoria: 1,
        promocion: true,
        descuento: 10,
        descripcion: "zapatos masculinos para adultos",
        nombreCategoria: "zapatos masculinos",
    },
];

// Llama a la función buscarCategoriaPorId con un ID y el callback de mostrarCategoria
buscarCategoriaPorId(categoria, 1, mostrarCategoria);
