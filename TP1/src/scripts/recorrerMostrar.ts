//Importación de archivos o librerias
import { ICategoria } from "../interfaces/ICategoria"; //llamar interfaz main xd
//---------------------------------------------------
// Creación de la función para mostrar el arreglo
export function recorrerMostrar(categorias: ICategoria[])
{
    //---------------------------------------
        // Usando forEach
        console.log("----Usando forEach:----");
        for (let i = 0; i < 3; i++) {
          categorias.forEach((categorias) => 
          {
            console.log("Ciclo de forEach #",i)
            console.log("Nombre de Categoría:", categorias.nombreCategoria);
            console.log("ID de Categoría:", categorias.idCategoria);
            console.log("Promoción activa:", categorias.promocion);
            console.log("Posee un descuento del:", categorias.descuento,"%");
            console.log("Descripción:", categorias.descripcion);
            console.log("-----------------");
          });
        }
        

    // Usando for...in
    console.log("\n----Usando forIn:----");
    for (let i = 0; i < 3; i++) {
      for (const indice in categorias) 
      {
          const categoria = categorias[indice];
          console.log("Ciclo de forIn #",i)
          console.log("Nombre de Categoría:", categoria.nombreCategoria);
          console.log("ID de Categoría:", categoria.idCategoria);
          console.log("Promoción activa:", categoria.promocion);
          console.log("Posee un descuento del:", categoria.descuento,"%");
          console.log("Descripción:", categoria.descripcion);
          console.log("-----------------");
      }
    }


    // Usando for...of
    console.log("\n-----Usando forOf:------");
    for (let i = 0; i < 3; i++) {
      for (const categoria of categorias) 
      {
          console.log("Ciclo de forOf #",i)
          console.log("Nombre de Categoría:", categoria.nombreCategoria);
          console.log("ID de Categoría:", categoria.idCategoria);
          console.log("Promoción activa:", categoria.promocion);
          console.log("Posee un descuento del:", categoria.descuento,"%");
          console.log("Descripción:", categoria.descripcion);
          console.log("-----------------");
      }
    }
}