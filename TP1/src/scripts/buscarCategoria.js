// función que busca un elemento por su id que coloquemos y luego utiliza un callback para mostrarlo
function buscarCategoriaPorId(categorias, idCategoria, callback) {
    const categoriaEncontrada = categorias.find(elemento => elemento.idCategoria === idCategoria);
    callback(categoriaEncontrada);
}

// función de callback para mostrar el arreglo encontrado por consola
function mostrarCategoria(categoria) {
    if (categoria) { //en caso de encontrar el id se muestran los datos del arreglo
        console.log("Elemento encontrado:");
        console.log("ID:", categoria.idCategoria);
        console.log("Nombre:", categoria.nombreCategoria);
        console.log("Descuento activo:",categoria.promocion);
        console.log("Cuenta con un descuento del:",categoria.descuento,"%");
        console.log("Descripción:",categoria.descripcion);
    } else { //en caso de no encontrar el id se muestra este mensaje
        console.log("Elemento no encontrado");
    }
}

// Exporta la función
module.exports = {
    buscarCategoriaPorId,
    mostrarCategoria
};
