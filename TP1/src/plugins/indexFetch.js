import { httpClient } from "./httpClients";
// Función para obtener información de un Pokémon por su ID
function getPokemonById(pokemonId) {
    // Construir la URL de la API de Pokémon con el ID proporcionado
    const apiUrl = `https://pokeapi.co/api/v2/pokemon/${pokemonId}/`;
  
    // Realizar una solicitud GET utilizando fetch
    fetch(apiUrl)
      .then(response => {
        // Verificar si la solicitud fue exitosa (código de respuesta 200)
        if (!response.ok) {
          throw new Error('La solicitud no fue exitosa');
        }
        // Parsear la respuesta JSON
        return response.json();
      })
      .then(data => {
        // imprimir datos del pokemon
        console.log('Nombre del Pokémon:', data.name);
        console.log('Altura:', data.height);
        console.log('Peso:', data.weight);
        console.log('Tipos:');
        // Recorrer y mostrar los tipos del Pokémon
        data.types.forEach(type => {
          console.log(type.type.name);
        });
      })
      //error
      .catch(error => {
        console.error('Ocurrió un error:', error);
      });
  }
  
  // Llamar a la función 
  getPokemonById(5); 

