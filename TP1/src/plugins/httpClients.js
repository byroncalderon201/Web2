//inicio de la petición
const httpClient = {
    get: async (url) => {
        try {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error('No se pudo completar la solicitud');
            }
            const data = await response.json();
            return data;
        } catch (error) {
            throw error;
        }
    },
    //
    post: (url, body) => {},
    put: (url, body) => {},
    delete: (url, body) => {},
};
//exportar el modulo
module.exports = { httpClient };
